# Housing Values in Suburbs of Boston

The  **medv**  variable is the target variable.

### Data description

The Boston data frame has 506 rows and 14 columns.

This data frame contains the following columns:

**_crim_**  
per capita crime rate by town.

**_zn_**  
proportion of residential land zoned for lots over 25,000 sq.ft.

_**indus**_  
proportion of non-retail business acres per town.

_**chas**_  
Charles River dummy variable (= 1 if tract bounds river; 0 otherwise).

_**nox**_  
nitrogen oxides concentration (parts per 10 million).

_**rm**_  
average number of rooms per dwelling.

_**age**_  
proportion of owner-occupied units built prior to 1940.

_**dis**_  
weighted mean of distances to five Boston employment centres.

_**rad**_  
index of accessibility to radial highways.

_**tax**_  
full-value property-tax rate per  $10,000.

_**ptratio**_  
pupil-teacher ratio by town.

_**black**_  
1000(Bk - 0.63)^2 where Bk is the proportion of blacks by town.

_**lstat**_  
lower status of the population (percent).

_**medv**_  
median value of owner-occupied homes in  $1000s.

### Source

Harrison, D. and Rubinfeld, D.L. (1978) Hedonic prices and the demand for clean air. J. Environ. Economics and Management 5, 81–102.

Belsley D.A., Kuh, E. and Welsch, R.E. (1980) Regression Diagnostics. Identifying Influential Data and Sources of Collinearity. New York: Wiley.


### Zadatak 1.
Generiraju se umjetni podaci, izgrađuje linearni regresijski model oblika y = theta0 + theta1*x koristeci gotovu python funkciju te se sve prikazuje na grafu.

### Zadatak 2.
Uz pomoć priložene forme u predlošku određuje se vrijednost parametara theta. Vrijednosti koje se dobiju jednaku se vrijednostima parametara linearnog modela iz zadatka 1.

### Zadatak 3.
Uz pomoć priloženog algoritma u predlošku kreirao sam metodu gradijentnog spusta za određivanje parametara 
linearnog modela. Povećanjem alpha vrijednosti stopa učenja postaje previsoka, dolazak do minimuma je prebrz, odnosno koraci kojima to radimo postaju preveliki te model daje neispravne rezultate.

### Zadatak 4.
Veći stupanj polinoma znaći da će model bolje aproksimriati zadanu funkciju iako ne treba pretjerati s stupnjem polinoma.

### Zadatak 5.
Povećanje stupnja polinoma daje bolju aproksimaciju zadane funkcije. No za preveliki stupanj moguće je pojavljivanje overfittinga tako da je potrebno pronaći kompromis. U ovom slučaju to je vrijednost stupnja 6. 

### Zadatak 6.
Povećavanjem regulacijskog parametra model pokazuje lošije rezulate kod trening parametara, no bolje na 
testnim podacima. 