import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error

def non_func(x):
	y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
	return y

def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy

def line(x, theta):
    return theta[1]*x + theta[0]

def criterionFunction(x,y,theta):
    J = 0.0
    n = x.shape[0]
    for i in range(0,n):
        J += (line(x[i],theta) - y[i]) ** 2
    
    J /= (2*n)

    return J

x = np.linspace(1,10,100)
y_true = non_func(x)
y_measured = add_noise(y_true)

np.random.seed(12)
indeksi = np.random.permutation(len(x))
indeksi_train = indeksi[0:int(np.floor(0.7*len(x)))]
indeksi_test = indeksi[int(np.floor(0.7*len(x)))+1:len(x)]

x = x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis]

xtrain = x[indeksi_train]
ytrain = y_measured[indeksi_train]

n_samples = xtrain.shape[0]

X = np.ones((n_samples,1))
X = np.append(X, xtrain, axis=1)

theta_direct = np.linalg.inv(np.transpose(X) @ X) @ np.transpose(X) @ ytrain

xp = np.array([xtrain.min(), xtrain.max()])
yp = np.array([line(xp[0],theta_direct), line(xp[1],theta_direct)])

no_iter = 200
theta_old = np.zeros((2,1))
theta_new = np.zeros((2,1))
dulj_koraka = 0.05
J = np.zeros((no_iter,1))
alphas = [0.01, 0.03, 0.05, 0.07]

j = 0
for alpha in alphas:
    j +=1
    plt.figure(j)
    plt.scatter(xtrain, ytrain, marker='.')   
    plt.title("gradient descent")

    for iter in range(0, no_iter):

        J[iter] = criterionFunction(xtrain,ytrain,theta_old)
            
        rj0 = 0.0
        rj1 = 0.0

        for i in range(0,n_samples):
            rj0 += line(xtrain[i],theta_old) - ytrain[i]
            rj1 += (line(xtrain[i],theta_old) - ytrain[i])*xtrain[i]

        rj0 /= n_samples
        rj1 /= n_samples

        theta_new[0] = theta_old[0] - alpha * rj0
        theta_new[1] = theta_old[1] - alpha* rj1
        theta_old = theta_new

        yp[0] = line(xp[0],theta_new)
        yp[1] = line(xp[1],theta_new)
    
    plt.plot(xp,yp,label = 'alfa = ' + str(alpha))
    plt.legend()
    print(J[0], "\n")
    print(theta_old, "\n")

    plt.figure(8)
    plt.plot(range(len(J)), J, label = 'alfa = ' + str(alpha))
    plt.ylabel('Vrijednost kriterijske funkcije')
    plt.xlabel('iteracija')

plt.ylim((0.5, 1))  
plt.legend()
plt.show()






