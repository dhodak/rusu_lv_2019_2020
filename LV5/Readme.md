#### Zadatak 1

Prilikom pokretanja napisanog koda više puta mogu se pojaviti različita riješenja s obzirom
da je kmeans algoritam osjetljiv na početni odabir centra. 



#### Zadatak 2
Primjećuje se da prilikom povećavanja broja clustera  vrijednost kriterijske funckije opada što je
normalno jer su clusteri gušći i točke su bliže jedna drugoj. 
Metodom "lakta" možemo pronaći optimalni broj clustera , tako da pronađemo onu vrijednost gdje 
kriterijska funkcija ima najveću promjenu nagiba.

#### Zadatak 3 
Uz pomoć prikazanih dendograma možemo lako odrediti optimlani broj clustera. 


#### Zadatak 4
Manji broj clustera izrazito utječe na kvalitetu slike, tek broj clustera od 10 sliku prikazuje
veoma sličnoj izvornoj s manjim nepravilnostima u bojama i nešto je tamnija. 

#### Zadatak 5
Slični rezultati kao i u prethodnom zadatku, uz vidljivu manju kvalitetu slike pri manjem broju
clustera te pri cluster = 10 slika koja je gotovo ista originalu. 