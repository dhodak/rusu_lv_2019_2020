import scipy as sp
from sklearn import cluster, datasets
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimage

image = mpimage.imread('D:/Projects/rusu_lv_2019_2020\LV5/resources/example.png')


X = image.reshape((-1, 1))  # We need an (n_sample, n_feature) array

plt.figure()
plt.title('Original image')
plt.imshow(image, cmap='gray')

for i in range(2, 11):
    k_means = cluster.KMeans(n_clusters=i, n_init=1)
    k_means.fit(X)
    values = k_means.cluster_centers_.squeeze()
    labels = k_means.labels_
    image_compressed = np.choose(labels, values)
    image_compressed.shape = image.shape

    plt.figure()
    plt.title('Number of clusters %d' %(i))
    plt.imshow(image_compressed, cmap='gray')

plt.show()