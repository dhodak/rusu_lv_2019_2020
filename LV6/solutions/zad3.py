import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
import zad1

X_train = zad1.train_dataset
X_test = zad1.test_dataset

logReg = lm.LogisticRegression()
logReg.fit(X_train[:, 0:2], X_train[:, 2])

xp = np.array([X_train[:, 0].min(), X_train[:, 0].max()])
yp1 = -logReg.coef_[0][0] / logReg.coef_[0][1] * xp[0] - logReg.intercept_[0] / logReg.coef_[0][1]
yp2 = -logReg.coef_[0][0] / logReg.coef_[0][1] * xp[1] - logReg.intercept_[0] / logReg.coef_[0][1]
yp = np.array([yp1, yp2])

plt.scatter(X_train[:, 0], X_train[:, 1], c=X_train[:, 2])
plt.plot(xp, yp)
plt.title("Scikit learn logisticka regresija")
plt.xlabel('x1')
plt.ylabel('x2')
plt.show()
