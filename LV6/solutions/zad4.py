import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
import zad1

X_train = zad1.train_dataset
X_test = zad1.test_dataset

logReg = lm.LogisticRegression()
logReg.fit(X_train[:, 0:2], X_train[:, 2])

f, ax = plt.subplots(figsize=(8, 6))
x_grid, y_grid = np.mgrid[min(X_train[:,0])-0.5:max(X_train[:,0])+0.5:.05,
                          min(X_train[:,1])-0.5:max(X_train[:,1])+0.5:.05]
grid = np.c_[x_grid.ravel(), y_grid.ravel()]
probs = logReg.predict_proba(grid)[:, 1].reshape(x_grid.shape)

cont = ax.contourf(x_grid, y_grid, probs, 60, cmap="Greys", vmin=0, vmax=1)
ax.scatter(X_train[:,0], X_train[:,1], c = X_train[:,2], cmap= 'YlGn')
ax_c = f.colorbar(cont)
ax_c.set_label("$P(y = 1|\mathbf{x})$")
ax_c.set_ticks([0, .25, .5, .75, 1])
ax.set_xlabel('$x_1$', alpha=0.9)
ax.set_ylabel('$x_2$', alpha=0.9)
ax.set_title('Izlaz logisticke regresije')
plt.show()
