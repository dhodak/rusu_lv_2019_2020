import numpy as np
import matplotlib.pyplot as plt
import sklearn.metrics as metrics
import sklearn.linear_model as lm
import zad1

def plot_confusion_matrix(c_matrix):
    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j) / float(a))
        norm_conf.append(tmp_arr)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')

    width = len(c_matrix)
    height = len(c_matrix[0])

    for x in range(width):
        for y in range(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x),
                        horizontalalignment='center',
                        verticalalignment='center', color='green', size=20)

    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])

    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    plt.show()


X_train = zad1.train_dataset
X_test = zad1.test_dataset

logReg = lm.LogisticRegression()
logReg.fit(X_train[:, 0:2], X_train[:, 2])

predictions = logReg.predict(X_test[:, 0:2])

plot_confusion_matrix(metrics.confusion_matrix(X_test[:, 2], predictions))

tn, fp, fn, tp = metrics.confusion_matrix(X_test[:, 2], predictions).ravel()
specificity = tn / (tn + fp)

print('Accuracy = %.2f' % metrics.accuracy_score(X_test[:, 2], predictions))
print('Precision = %.2f' % metrics.precision_score(X_test[:, 2], predictions))
print('Missclassification rate = %.2f' % (1 - metrics.accuracy_score(X_test[:, 2], predictions)))
print('Specificity = %.2f' % specificity)
print('Recall = %.2f' % metrics.recall_score(X_test[:, 2], predictions))


