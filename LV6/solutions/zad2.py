import numpy as np
import matplotlib.pyplot as plt
import zad1

X = zad1.train_dataset

plt.scatter(X[:, 0], X[:, 1], c=X[:, 2])
plt.xlabel('x1')
plt.ylabel('x2')
plt.show()