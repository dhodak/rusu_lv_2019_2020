import urllib.request
import pandas as pd
import xml.etree.ElementTree as ET
import matplotlib.pyplot as plt

# url that contains valid xml file:
url = 'http://iszz.azo.hr/iskzl/rs/podatak/export/xml?postaja=160&polutant=5&tipPodatka=4&vrijemeOd=1.1.2017&vrijemeDo=31.12.2017.'

airQualityHR = urllib.request.urlopen(url).read()


root = ET.fromstring(airQualityHR)

df = pd.DataFrame(columns=('mjerenje', 'vrijeme'))

i = 0
while True:
    
    try:
        obj =  list(list(root)[i])
    except:
        break
    
    row = dict(zip(['mjerenje', 'vrijeme'], [obj[0].text, obj[2].text]))
    row_s = pd.Series(row)
    row_s.name = i
    df = df.append(row_s)
    df.mjerenje[i] = float(df.mjerenje[i])
    i = i + 1

df.vrijeme = pd.to_datetime(df.vrijeme, utc = True)
df.plot(y='mjerenje', x='vrijeme');

# add date month and day designator
df['month'] = df['vrijeme'].dt.month
df['dayOfweek'] = df['vrijeme'].dt.dayofweek

print("Top 3 dates with most particles in air:\n",df.sort_values(['mjerenje'], ascending = False).head(3).vrijeme.dt.date)

daysInMonths = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
missingDays = df.groupby('month').count()
missingDays['dayOfweek'] = abs(missingDays.dayOfweek - daysInMonths)
missingDays.plot.bar(y = 'dayOfweek')

concentration = df[(df.month == 2) | (df.month == 7)]
concentration.boxplot(by ='month', column = 'mjerenje')

df['weekend'] = (df.dayOfweek >= 5) & (df.dayOfweek <= 6)
df.boxplot(by = 'weekend', column = 'mjerenje')

plt.show()



