import pandas as pd
import numpy as np


mtcars = pd.read_csv("LV3/resources/mtcars.csv")

print(mtcars)

mtcars_mpg = mtcars.sort_values('mpg', ascending = False)
print("Five cars with highest mpg:\n",mtcars_mpg.head(5), "\n")

cars_EightCly = mtcars[mtcars.cyl == 8].sort_values('mpg', ascending = False)
print("Three cars with 8 cylinders and lowest consumption:\n",cars_EightCly.tail(3), "\n")

cars_SixCly = mtcars[mtcars.cyl == 6].sort_values('mpg')
print("Mean mpg of cars with 6 cylinders is:", np.mean(cars_SixCly['mpg']))

cars_FourCly = mtcars[(mtcars.cyl == 4) & (mtcars.wt <= 2.2) & (mtcars.wt >= 2)]
print("Mean mpg of cars with 4 cylinders is:", np.mean(cars_FourCly['mpg']))

print("Number of cars with automatic transmission is ", len(mtcars[mtcars.am == 0]))
print("Number of cars with automatic transmission and horse power over 100 is ", len(mtcars[(mtcars.am == 0) & (mtcars.hp > 100)]), "\n")

cars_kg = mtcars
cars_kg['wt'] = cars_kg.wt* 1000 * 0.45359237
print("Conversion in kg:\n", cars_kg)

