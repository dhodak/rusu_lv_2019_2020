import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv("LV3/resources/mtcars.csv")

cyl_cars = mtcars.groupby('cyl')
cyl_cars_mean = cyl_cars.mean()
cyl_cars_mean.plot.bar(y='mpg')


mtcars.boxplot(by = 'cyl', column = 'wt')

mtcars.boxplot(by = 'am', column = 'mpg')

mtcars.boxplot(by = 'am', column = 'hp')

mtcars.boxplot(by = 'am', column = 'qsec')

plt.show()