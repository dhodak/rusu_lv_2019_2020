### mtcars.csv

Motor Trend Car Road Tests

The data was extracted from the 1974 Motor Trend US magazine, and comprises fuel consumption and 10 aspects of automobile design and performance for 32 automobiles (1973–74 models)
* mpg - Miles/(US) gallon
* cyl - Number of cylinders
* disp - Displacement (cu.in.)
* hp - Gross horsepower
* drat - Rear axle ratio
* wt - Weight (lb/1000)
* qsec - 1/4 mile time
* vs - V/S
* am - Transmission (0 = automatic, 1 = manual)
* gear - Number of forward gears
* carb - Number of carburetors

Source: R built in dataset



### AirQualityRH.py

Skripta za dohvaćanje podataka o kvaliteti zraka pomocu REST API


Zad 1.

Bilo je potrebno isprobati primjere u predlošku što je bilo dovoljno da bi se zadatak uspješno riješio.
Korišteni su uvjeti unutar uglatih zagrada kod poziva dataframea kako bi se pristupilo željenim podacima te len funkcija za broj slučajeva.

Zad 2.
Prvotno se dataframe grupirao po cyl što nam je omogućilo da odredimo srednje vrijednosti za svaki stupac te se onda za bar plot trebao izdvojiti samo stupac mpg.

Zad 3.
Ispravljeni su bugovi u importu, deprecated root.getchildren funkcija i pretvorba vremena u datetime.
Za  3 datuma s najviše čestica sortirao se dataframe po mjerenjima i prijemnila funkcija head.
Za izostale dane dataframe se grupirao po mjescima te se stupac daysOfweek oduzeo od liste s standarnim brojem dana u mjesecima.
Za distribuciju se koristio boxplot gdje je kreiran dodatni stupac u dataframeu koji predstavlja vikend i ima vrijednost true ili false u ovisnosti je li dan u vikendu ili ne. 
