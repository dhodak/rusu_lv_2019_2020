import numpy as np
import matplotlib.pyplot as plt

def throwDice(value):
    result = np.random.randint(low = 1, high= 7, size = value)
    return result
        
print(throwDice(100))

throws = throwDice(100)

plt.hist(throws, bins = 15, color =(0,0,1))
plt.ylabel("Frequency")
plt.xlabel("Number of throws")
plt.show()