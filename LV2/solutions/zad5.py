import csv
import numpy as np
import matplotlib.pyplot as plt

cars = []
values = np.array((50,2))
mpgs = []
hps = []

with open('resources/mtcars.csv', mode='r') as data:
    for line in csv.DictReader(data):
        cars.append(line)

for car in cars:
    mpgs.append((car["mpg"]))
    hps.append(car["hp"])


hps = [int(i) for i in hps]
mpgs = [float(i) for i in mpgs]

hps.sort()
mpgs.sort(reverse = True)

plt.plot(hps, mpgs, 'r')
plt.ylabel("Mile per gallon")
plt.xlabel("Horsepower")
plt.show()

print(np.mean(mpgs))
print(np.min(mpgs))
print(np.max(mpgs))


