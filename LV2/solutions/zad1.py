import re
import numpy as np

fileName = "resources/mbox-short.txt"

usernames = []

try:
  with open(fileName) as file:
    data = file.read()
      
  emails = re.findall(r"[\w\.-]+@[\w\.-]+\.\w+", data)
  emails = np.unique(emails)

  for email in emails:
    usernames.append(email.split("@")[0])

except IOError:
    print("File is not accessible")
      
print(usernames)
   
