import re
import numpy as np

fileName = "resources/mbox-short.txt"

try:
    with open(fileName) as file:
        data = file.read()

    
    emails = re.findall(r"[\w\.-]+@[\w\.-]+\.\w+", data)
    emails = np.unique(emails)

    usernames = " ".join(emails)
  
    print(usernames, "\n")
    
    print(re.findall(r"(\S*a+\S*)@", usernames), "\n")
    print(re.findall(r"([b-zB-Z0-9\.-_]*a[b-zB-Z0-9\.-_]*)@|\S*@", usernames), "\n")
    print(re.findall(r"([b-zB-Z0-9\.-_]*)@|\S*@",usernames), "\n")
    print(re.findall(r"(\S*[0-9]+\S*)@", usernames), "\n")
    print(re.findall(r"([a-z]+)@|\S*@", usernames))
   
except IOError:
    print("File is not accessible")