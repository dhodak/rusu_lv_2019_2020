import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm

np.random.seed(34)

people = np.random.randint(2, size=1000)

heightsM = []
heightsF = []

for number in people:
    if(number == 1):
        heightsM.append(np.random.normal(180, 7))
       
    else:
        heightsF.append(np.random.normal(167, 7))

 
plt.grid(True)

plt.figure(1)
plt.hist(heightsM, bins = 50, color =(0,0,1))
plt.hist(heightsF, bins = 50,  color =(1,0,0))
plt.title("Histogram")
plt.xlabel("Height")
plt.ylabel("Frequency")

avgm = np.mean(heightsM)
avgf = np.mean(heightsF)

plt.axvline(avgm, color="blue")
plt.axvline(avgf, color="red")


domainM = np.linspace(140, 220, 500)
domainF = np.linspace(140, 220, 500)
plt.figure(2)
plt.plot(domainM,  norm.pdf(domainM, np.mean(heightsM), np.std(heightsM)), 'b')
plt.plot(domainF,  norm.pdf(domainF, np.mean(heightsF), np.std(heightsF)), 'r')
plt.title("Standard normal")
plt.xlabel("Height")
plt.ylabel("Density")


plt.show()


