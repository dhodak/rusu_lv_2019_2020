Ovaj repozitorij sadrži python skriptu koja broji koliko puta se pojedina riječ pojavljuje u tekstualnoj datatoteci. Rezultat bi trebao biti dictionary gdje uz svaku riječ (key) stoji broj ponavljanja riječi (value). Međutim, skripta ima bugove i ne radi kako je zamišljeno.

raw_input je promijenjen u input
print funkcija je napisana ispravno u oba slučaja, zagrade su nedostajale
greška u nazivu varijable fname