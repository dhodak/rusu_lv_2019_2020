def categorizeGrade(value):
    if value >= 0.9:
        return 'A'
    elif value >= 0.8:
        return 'B'
    elif value >= 0.7:
        return 'C'
    elif value >= 0.6:
        return 'D'
    else:
        return 'F'


try:
    _grade = float(input("Enter a grade in interval[0.0, 1.0]: "))

    if _grade < 0.0 or _grade > 1.0:
        raise RuntimeError("Value is not in interval [0.0, 1.0]")

    print("Grade is " + categorizeGrade(_grade))

except RuntimeError as er:
    print(er)
except ValueError:
    print("Not a number")