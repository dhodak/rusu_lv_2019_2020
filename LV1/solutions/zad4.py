def meanValue(numbers):
    summ = 0
    for numb in numbers:
        summ += numb

    if summ == 0:
        return 0
    else:
        return summ / len(numbers)


_numbers = []

print("Enter a word Done if you want to finish a loop")

while(1):
    try:
        _numb = input("Enter a number or Done: ")

        if _numb == "Done":
            break

        _numbers.append(int(_numb))
    except ValueError:
        print("Not a number")

try:
    print("User has entered ", len(_numbers), " numbers")
    print("Mean size is ", meanValue(_numbers))
    print("Maximum value is ", max(_numbers))
    print("Minimum value is ", min(_numbers))
except ValueError:
    print("Min and max values are zero")