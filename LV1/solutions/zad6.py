_fileName = input("Enter a file name : ")
_string_to_search = "From "

_mailAddresses = []
_hostnames = []

mailDict = {}

try:
    _file = open(_fileName)

    for line in _file:
        if _string_to_search in line:
            _mailAddresses.append(line.split(" ")[1].rstrip('\n'))
            
    for mail in _mailAddresses:
        _hostnames.append(mail.split("@")[1])
      
    for mail in _hostnames:
        count = 0
        for m in _hostnames:
            if mail == m:
                count += 1
        mailDict[mail] = str(count)
    
              
    _file.close()
    
    print(mailDict)

except IOError:
    print("File is not accessible")